# qa service

## project details

This project has been set up with the intent to
* support the testing efforts of the QA team,
* specifically provide runtime validation of offers made in production (future),
* migrate business logic of the offer stage away from the cypress testing suite,
* explore stable, cheap, cloud-native ways of exposing the business logic,
* depreacte the internal tooling hosted under ops-utils and increase maintainability.

Most aspects of the offer phase are made available through endpoints or are under development:
* company selection based on filters (aligned with `new10-test` mock KvK data);
* generation of manual input values;
* generation of CAMT/MT transaction files;
* evaluation of policy rules;
* calculation of risk classes;
* price calculation;
* redemption capacity calculation;
* best fit calculation.

## stages

The project has the following [stages](./stages) :

Stage | AWS account | Stack name | SNS | S3 |
--- | --- | --- | --- | --- 
*dev* | `new10-sandbox` | **qa-service-dev** |*qa_notify_dev*|*sls-dev-qa-service*
*test* | `new10-dev`     | **qa-service-test** |*qa_notify_test*|*sls-test-qa-service*

The SNS topic and the S3 bucket were set up manually (later to be set up through `terraform`).

## infrastructure

The service's infrastructure is relatively simple:  
* a total of 8 endpoints are exposed through the [api gateway](#api-gateway);
* each endpoint triggers a separate lambda holding the business logic;
* all settings needed for the calculation are stored in one [S3 bucket](#s3);
* to keep price and best-fit offers up-to-date, two more lambdas are triggered by updates in S3;
* these two lambdas report to a [SNS topic](#sns), to provide a timely summary of their action.

### api gateway

Endpoint                                  | Method  | Description
---                                         | ---     | ---
[companies](#companies)                     | POST    | returns a list of companies (and their properties) available in the mock-kvk database
[manualinput](#manualinput)                 | POST    | returns the fields needed for the manual input page
[transactionfile](#transactionfile)         | POST    | returns a transaction file suited for an application
[policies](#policies)                       | POST    | returns the outcome of testing the (credit risk) policies
[riskscore](#riskscore)                     | POST    | returns the risk score/class/PD, given a set of risk drivers
[price](#price)                             | POST    | returns the detail of the price calculation (interest rate, first repayment and all intermediary values)
[redemptioncapacity](#redemptioncapacity)   | POST    | returns the monthly redemption capacity of the client
[bestfit](#bestfit)                         | POST    | returns a fitting size and term given the available redemption capacity 

#### companies

In the body of the request, all keys are optional, and act as filters if included. Beware that some combinations of filters do not correspond to any company in the database and will return an empty response.  

  Key | Type | Optional | Comment
---   | ---  | ---      | ---
`min age months` | int | yes |
`max age months` | int | yes |
`GCRM risk class`| str | yes | can be `3:HR`, `2:MR` or `1:LR`
`legal form`     | str | yes | can be `BV`, `EMZ` or `VOF`
`complex`        | bool| yes | select complex structures using `true` or simple `false`

**example:**

_request:_
```json
{
    "min age months": 50,
    "max age months": 300,
    "GCRM risk class": "3:HR",
    "legal form": "BV",
    "complex": false
}
```
_response:_
```json
{
    "Human Angle B.V.": {
        "founded": "2012-07-23",
        "legal form": "BV",
        "name": "Human Angle B.V.",
        "sbi": "78202",
        "zip": "1071HM",
        "complex": false,
        "gcrm risk": "3:HR",
        "age in months": 68
    },
    "MacRelife Bergen op Zoom B.V.": {
        "founded": "2017-09-28",
        "legal form": "BV",
        "name": "MacRelife Bergen op Zoom B.V.",
        "sbi": "47912",
        "zip": "4611NB",
        "complex": false,
        "gcrm risk": "3:HR",
        "age in months": 6
    }
}
```
#### manualinput

In the body of the request, all keys are optional. If no body is provided, a reposnse is generated with the default settings.

  Key                           | Type | Optional | Default (small/medium)| Comment
---                             | ---  | ---      | ---     | ---
`Scorecard`                     | str  | yes      | `small` | `small` or `medium` 
`Result to Turnover class`      | str  | yes      | `A`     | `A`-`D`/`A`-`D`; mapped to RETO preset 
`Solvency class`                | str  | yes      | `A`     | `A`-`E`/`A`-`C`; mapped to OAMTA preset
`Monthly redemption capacity`   | int  | yes      | `1150`/`5800`| Note that setting a high redemption capacity will also 'force' a high result, which could conflict with the `scorecard` setting.
`Reject on Loan to Others`      | bool | yes      | `false` |  
`Reject on Equity`              | bool | yes      | `false` |
`Reject on Turnover`            | bool | yes      | `false` |
`Reject on Profit`              | bool | yes      | `false` | 

**example:**

_request:_
```json
{
    "Scorecard": "medium",
    "Result to Turnover class": "A",
    "Solvency class": "A",
    "Monthly redemption capacity": 900,
    "Reject on Loan to Others": false,
    "Reject on Equity": false,
    "Reject on Turnover": false,
    "Reject on Profit": false
}
```
_response:_
```json
{
    "input": {
        "Scorecard": "medium",
        "Result to Turnover class": "A",
        "Solvency class": "A",
        "OAMTA": 0.3,
        "RETO": 0.1,
        "Reject on Loan to Others": false,
        "Reject on Equity": false,
        "Reject on Turnover": false,
        "Reject on Profit": false,
        "Monthly redemption capacity": 900
    },
    "Totaal Activa": 1000000,
    "Eigen Vermogen": 300000,
    "Aflossingsverplichtingen": 200,
    "Resultaat": 11200,
    "Netto omzet": 112000,
    "Dividend": 200,
    "Leningen aan Derden": 60000
}
```

#### transactionfile

  Key                           | Type | Optional | Scorecard | Default | Comment
---                             | ---  | ---      | ---  | ---     | ---
`format`                        | str  | yes      | both | `mt`    | `mt` or `camt` 
`size`                          | str  | yes      | both | `small` | `small` or `medium` 
`ALB`                   | int  | yes  | small/med | `40000`/`5000` | Average Ledger Balance; overridden by `albclass` 
`NTO`                           | int  | yes      | med  | `20000` | Net TurnOver;overridden by `ntoclass` 
`NDT`                           | int  | yes      | small| `100`   | Number of Debit Transactions; overridden by `ndtclass` 
`DID`                           | int  | yes      | small| `0`     | Days In Debt; overridden by `didclass` 
`CTO`                           | int  | yes      | small| `80000` | Credit TurnOver; overridden by `ctoclass` 
`CTOdev`                        | int  | yes      | small| `1.5`   | Credit TurnOver Development; overridden by `ctodevclass` 
`transSize`                     | int  | yes      | both | `1000`  | size of biggest transaction 
`numtransL3M`                   | int  | yes      | both | `1000`  | number of transactions in the last 3 months
`enddate`                       | str  | yes      | both | today   | date of most recent transaction; formatted yyyy-mm-dd
`ndays`                         | int  | yes      | both | `450`   | number of consecutive days of transactions
`iban`           | str  | yes      | both | `NL29INGB0642516537`   | iban of the account the transactions are reported from
`albclass`                      | str  | yes      | small/med | `A`| Scorecard class for Average Ledger Balance; `A`-`B`/`A`-`F`
`ntoclass`                      | str  | yes      | med  | `A`     | Scorecard class for Net TurnOver; `A`-`B`
`ndtclass`                      | str  | yes      | small| `A`     | Scorecard class for Number of Debit Transactions; `A`-`B`
`didclass`                      | str  | yes      | small| `A`     | Scorecard class for Days In Debt; `A`-`D`
`ctoclass`                      | str  | yes      | small| `A`     | Scorecard class for Credit TurnOver; `A`-`D`
`ctodevclass`                   | str  | yes      | small| `A`     | Scorecard class for Credit TurnOver Development; `A`-`C`
`download`                      | bool | yes      | both | `false` | when true, reduces the response to the file content only 

**example:**

_request:_
```json
{
	"format": "camt",
	"size": "medium",
	"NDT": 100,
	"NTO":10000,
	"ALB":5000,
	"CTOdev":0.5,
	"DID":50,
	"CTO":3000,
	"transSize": 1000,
	"numtransL3M":1000,
	"enddate":"2018-03-08",
	"ndays":450,
	"iban":"NL29INGB*****16537",
	"albclass":"B",
	"ntoclass":"B",
	"didclass":"B",
	"ctoclass":"B",
	"ndtclass":"B",
	"ctodevclass":"B",
	"download": false
}
```
_response:_
```json
{
    "build settings": {
        "ALB": 0,
        "NTO": 10000,
        "DID": 0,
        "CTO": 80000,
        "NDT": 100,
        "CTOdev": 1.5,
        "maxsize transaction": 1000,
        "numtrans L3M": 1000,
        "enddate": "2018-03-08",
        "ndays": 450
    },
    "write settings": {
        "fmt": "camt",
        "path": "./",
        "filename": "transaction_statement_",
        "days per statement": 450,
        "IBAN": "NL29INGB0642516537",
        "print": true
    },
    "body": {
        "file": "<?xml version=\"1.0\" ?>\n<Document xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" [...] </ValDt>\n\t\t\t\t<AcctSvcrRef>1234567890</AcctSvcrRef>\n\t\t\t</Ntry>\n\t\t</Stmt>\n\t</BkToCstmrStmt>\n</Document>\n",
        "extension": "xml",
        "values": {
            "AVG_Net_TO": 10000,
            "AVG_LEDG_BAL": 0
        }
    }
}
```

#### policies

The policies that are tested are stored in S3. The values provided under `values` are used to evaluate the policy rules. 
If not all needed values are provided, it skips the policy rule, and reports the rule as untested.

  Key                           | Type | Optional |Default | Comment
---                             | ---  | ---      | ---    | ---
`values`                        | json | yes      | `{}`   | contains the key-value pairs of values needed for the policy rules to be tested
`unsuccessful only`             | bool | yes      | `false`| filters the results for unsuccessful policy executions (skipped and failed)
`verbose`                       | bool | yes      | `true` | returns the full information about the input and execution of policies
`status`                        | bool | yes      | `false`| returns a summary of the execution; overrides `verbose` and `unsuccessful only`

**example 1:**

_request:_
```json
{
    "values":
	{
        "equity":1000,
    	"sbi":"85200",
    	"turnover": 200000,
    	"age": 15,
    	"loans_to_others":200
	},
    "unsuccessful only":true,
    "verbose":false,
    "status":false
}
```
_response:_
```json
{
    "2": {
        "failed": null,
        "message": "only profitable companies can request a loan",
        "missing dependencies": [
            "result"
        ]
    },
    "8": {
        "failed": true,
        "message": "unsupported business activities",
        "missing dependencies": []
    }
}
```

**example 2:**

_request:_
```json
{
    "values":
	{
        "equity":1000,
    	"sbi":"85200",
    	"turnover": 200000,
    	"age": 15,
    	"loans_to_others":200
	},
    "status": true
}
```
_response:_
```json
{
    "summary": {
        "total": 9,
        "success": 7,
        "failed": 1,
        "untested": 1,
        "accept": false
    },
    "untested rules": ["2"],
    "failed rules": [["8", "unsupported business activities", ["sbi[:3] in ['852','853','854','856']"]]],
    "missing dependencies": ["result"]
}
```

#### riskscore

Evaluates the provided risk drivers against the designated scorecard. If a risk driver is missing, the calculation is aborted.
The settings for the scorecard are stored in S3. 

  Key            | Type | Optional | Comment
---              | ---  | ---      | ---
`scorecard`      | str  | no       | `small` or `medium`
`risk_drivers`   | json | no       | key-value pairs for the selected scorecard

_`risk drivers` for the `small` scorecard_

  Key                               | Type | Optional | Comment
---                                 | ---  | ---      | ---
`Average_Ledger_Balance_6M`         | int  | no       | value for Average Ledger Balance (transactions)
`Number_of_days_in_debit_3M`        | int  | no       | value for Number of Days In Debit (transactions)
`Average_Monthly_Credit_Turnover_3M`| int  | no       | value for Average Monthly Credit Turnover (transactions)
`cto_Development_XM`                | float| no       | value for Credit turnover development (transactions)
`Number_of_Debit_Transactions_3M`   | int  | no       | value for Number of Debit Transactions (transactions)
`OAMTA`                             | int  | no       | value for Own and Associated Means to Total Assets (or solvency) (manual input)
`RETO`                              | int  | no       | value for Result over Turnover (manual input)
`Age_of_Company`                    | int  | no       | value for age of company, in months

_`risk drivers` for the `medium` scorecard_

  Key            | Type | Optional | Comment
---              | ---  | ---      | ---
`AVG_LEDG_BAL`   | int  | no       | value for Average Ledger Balance (transactions)
`AVG_Net_TO`     | int  | no       | value for Average Net Turnover (transactions)
`OAMTA`          | int  | no       | value for Own and Associated Means to Total Assets (or solvency) (manual input)
`RETO`           | int  | no       | value for Result over Turnover (manual input)
`SBI`            | str  | yes      | SBI code. If not provided, defaults to `GCRM`: `3:HR` 
`GCRM`           | str  | yes      | GCRM risk class: `3:HR` (default), `2:MR` or `1:LR`



**example 1:**

_request:_
```json
{
    "scorecard": "medium",
    "risk_drivers":{
	    "AVG_LEDG_BAL":-30000,
	    "AVG_Net_TO":100000,
	    "OAMTA":0.3,
	    "RETO":0,
	    "SBI":"82991",
	    "GCRM":"2:MR"
    }
}
```
_response:_
```json
{
    "risk score": 305,
    "risk class": "GOOD",
    "PD": 0.012,
    "trace": {
        "AVG_Net_TO": 17,
        "AVG_LEDG_BAL": -52,
        "OAMTA": 0,
        "RETO": -14,
        "GCRM": 14
    }
}
```
**example 2:**

_request:_
```json
{
    "scorecard": "small",
    "risk_drivers":{
    	"Average_Ledger_Balance_6M": 0,
    	"Number_of_days_in_debit_3M":20,
    	"Average_Monthly_Credit_Turnover_3M":30000,
    	"cto_Development_XM":1.2,
	    "Number_of_Debit_Transactions_3M":500,
    	"OAMTA":0.4,
	    "RETO":0.2,
    	"Age_of_Company":8
    }
}
```
_response:_
```json
{
    "risk score": 276,
    "risk class": "AVERAGE",
    "PD": 0.03,
    "trace": {
        "cto_Development_XM": 0,
        "Average_Monthly_Credit_Turnover_3M": 4,
        "Number_of_Debit_Transactions_3M": -15,
        "Number_of_days_in_debit_3M": -19,
        "Average_Ledger_Balance_6M": 0,
        "OAMTA": 3,
        "RETO": 9,
        "Age_of_Company": 0
    }
}
```

#### price

Given the product, risk class, laon term, loan size and the age of the company, the interest rate is calculated.
Validation against the product parameters is performed on the requested loan term and size.

  Key                   | Type | Optional | Default  | Comment
---                     | ---  | ---      | ---      | ---
`legal form`            | str  | yes      | `EMZ`    |can be `BV`, `EMZ` or `VOF`
`asset`                 | bool | yes      | `false`  |whether the loan is an asset loan or not
`risk class`            | str  | no       | `AVERAGE`|options are: `VERY_GOOD` `GOOD` `AVERAGE`
`term`                  | int  | no       |          |term of the loan
`size`                  | int  | no       |          |size of the loan
`age in months`         | int  | yes      | `50`     |age of the company (in months)

**example:**

_request:_
```json
{
	"legal form": "BV",
	"asset": true,
	"risk class": "VERY_GOOD",
	"term": 24,
	"size": 21301,
	"age in months": 16
}
```
_response:_
```json
{
    "product": "BV_ASSET_LOAN",
    "risk_class": "VERY_GOOD",
    "term": 24,
    "size": 21301,
    "startupConservatism": 0.01,
    "ftp": -0.0016,
    "operationalCosts": 0.013,
    "capitalCost": 0.0095,
    "margin": 0.005,
    "pd": 0.004,
    "ead": 1,
    "lgd": 0.55,
    "expectedLoss": 0.0022,
    "conservativeExpectedLoss": 0.00264,
    "lifetimeConservativeExpectedLoss": 0.003432,
    "interestRate": 0.039332,
    "closingFee": 213.01,
    "bailSize": 20000,
    "firstPrincipal": 888,
    "firstInterest": 73,
    "firstRepayment": 961
}
```

#### redemptioncapacity

Using the manual input and the legal form, this endpoint returns the monthly redemption capacity of the client.

  Key                       | Type | Optional | Comment
---                         | ---  | ---      | ---
`legal form`                | str  | no       | can be `BV`, `EMZ` or `VOF`
`Aflossingsverplichtingen`  | int  | no       | 
`Resultaat`                 | int  | no       | 
`Dividend`                  | int  | yes      | only required for `BV`

**example:**

_request:_
```json
{
    "legal form": "BV",
    "Aflossingsverplichtingen": 200,
    "Resultaat": 11200,
    "Dividend": 200
}
```
_response:_
```json
{
    "output": {
        "monthly redemption capacity": 900
    },
    "input": {
        "legal form": "BV",
        "Aflossingsverplichtingen": 200,
        "Resultaat": 11200,
        "Dividend": 200
    }
}
```

#### bestfit

Given the client's redemption capacity, and product request, the endpoint wil calculate a best fit offer, if possible.
Note that this implementation of the best fit allows for a more tailored best-fit than LCM. 
The offer might deviate from the Ne10 flow. But the best fit offered by the endpoint should be valid, also on New10.com .   

  Key                   | Type | Optional | Comment
---                     | ---  | ---      | ---
`legal form`            | str  | no       | can be `BV`, `EMZ` or `VOF`
`asset`                 | bool | no       | whether the loan is an asset loan or not
`risk_class`            | str  | no       | options are: `VERY_GOOD` `GOOD` `AVERAGE`
`requested_term`        | int  | no       | term of the loan
`requested_size`        | int  | no       | size of the loan
`monthly_redemption_capacity` | int | no  | monthly redemption capacity the client has
`upsell        `        | bool | no       | if set to `true`, allows an upsell offer (larger amount)

**example:**

_request:_
```json
{
  "legal form": "EMZ",
  "asset": false,
  "risk_class": "GOOD",
  "monthly_redemption_capacity": 700,
  "requested_size": 210000,
  "requested_term": 32,
  "upsell": false
}
```
_response:_
```json
{
    "result": "downsell",
    "Term": 36,
    "size": 21000
}
```

### S3

The S3 bucket has the following structure:

  Folder        | Content | Settings
  ---           | ---     | ---
**best-fit**    | precompiled first repayment for every product setting | Reduced redundancy
**companies**   | lsit of companies, SBI-GCRM risk mapping|
**price-input** | ftp, pricing constants, product mapping, risk class mapping, scenarios| trigger on *.xlsx and *.json
**risk**        | scorecard, scorecard calibration, policies|

Two triggers are active on the bucket:
* if a *.xlsx file is placed in **price-input**, it is converted to a ftp json file. A SNS notification is also sent after conversion;
* if a *.json file is placed in **price-input**, all files in **best-fit** are recompiled. A SNS notification is also sent after the action is completed.

### sns

A SNS topic is created to broadcast changes in the files in S3. For now only an email is sent to me!
Future development will be aimed at integrating with slack instead.