import json

def make_manual_input(event, context):
    event = {} if event['body'] == None else json.loads(event['body'])
    settings=get_settings(event)
    return return_response(200,settings)

def get_settings(event):
    presets={'dividend':200,
             'redemption obligations':200,
             'scorecard':'small',
             'total assets':1000000}

    OAMTAratio = {  'small':  {'A': 0.9, 'B': 0.7, 'C': 0.4, 'D': 0.2, 'E':0.1},
                    'medium': {'A':0.3, 'B':0.1, 'C':0.02}}
    RETOratio = {   'small':  {'A': 0.2, 'B': 0.1, 'C': 0.04, 'D': 0.025},
                    'medium': {'A':0.1, 'B':0.03,'C':-0.03, 'D':-0.1}}
    
    result= {'input':{}}
    result['input']['scorecard']=return_val_if_present_and_correct(event,'scorecard',['small','medium'],presets['scorecard'])
    presets['redemption_capacity_month']=5800 if result['input']['scorecard']=='medium' else 1150

    result['input']['result_to_turnover_class']=return_val_if_present_and_correct(event,'result_to_turnover_class',RETOratio[result['input']['scorecard']].keys(),'A')
    result['input']['solvency_class']=return_val_if_present_and_correct(event,'solvency_class',OAMTAratio[result['input']['scorecard']].keys(),'A')
    result['input']['OAMTA']=OAMTAratio[result['input']['scorecard']][result['input']['solvency_class']]
    result['input']['RETO'] = RETOratio[result['input']['scorecard']][result['input']['result_to_turnover_class']]
    result['input']['reject_on_loan_to_others']=bool(return_val_if_present_and_correct(event,'reject_on_loan_to_others',[True,False],False))
    result['input']['reject_on_equity']=bool(return_val_if_present_and_correct(event,'reject_on_equity',[True,False],False))
    result['input']['reject_on_turnover']=bool(return_val_if_present_and_correct(event,'reject_on_turnover',[True,False],False))
    result['input']['reject_on_profit']=bool(return_val_if_present_and_correct(event,'reject_on_profit',[True,False],False))
    result['input']['redemption_capacity_month'] = return_val_if_present_and_correct(event,'redemption_capacity_month', [], presets['redemption_capacity_month'])

    result['Totaal Activa']=presets['total assets']
    result['Eigen Vermogen']=int(result['input']['OAMTA']*presets['total assets'])
    if result['input']['reject_on_equity']: result['Eigen Vermogen']=-result['Eigen Vermogen']
    result['Aflossingsverplichtingen']=presets['redemption obligations']

    result['Resultaat'] = -10000 if result['input']['reject_on_profit'] else 12*result['input']['redemption_capacity_month']+presets['redemption obligations']+presets['dividend']
    result['Netto omzet']= 89000 if result['input']['reject_on_turnover'] else int(abs(result['Resultaat'])/result['input']['RETO'])
    result['Dividend']=presets['dividend']

    result['Leningen aan Derden']=abs(int(0.3*result['Eigen Vermogen'])) if result['input']['reject_on_loan_to_others'] else abs(int(0.2*result['Eigen Vermogen']))
    return result

def return_val_if_present_and_correct(event,key,expected,default):
    if key in event:
        if event[key] in expected or (len(expected)==0 and type(event[key])==int):
            return event[key]
    return default

def return_response(code, val):
    return {'statusCode': code, 'body': json.dumps(val),
            'headers': {'Content-Type': 'application/json'}}