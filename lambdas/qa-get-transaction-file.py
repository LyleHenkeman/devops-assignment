from random import randint
from datetime import date, timedelta
import datetime, io, zipfile, copy, json
import xml.etree.ElementTree as ET
import xml.dom.minidom as MD

def make_transaction(event, context):
    event={} if event['body']==None else json.loads(event['body'])

    IBANlist={
        'ING':  'NL26INGB0755067437',
        'ABN':  'NL83ABNA0896616917',
        'RABO': 'NL23RABO0323326443',
        'SNS':  'NL47SNSB0909392160',
        'KNAB': 'NL60KNAB0417164300',
        'TRIO': 'NL79TRIO0391380036',
        'AEGON':'NL10AEGO0738812025',
        'TWKY': 'NL24RABO0135064945'
    }

    # get version of scorecard
    size = event['scorecard'] if 'scorecard' in event else 'small'
    # define presets
    presets = {'scorecard':   size,
               'bank':   'ING',
               'NDT':    {'A': 100, 'B': 200},
               'NTO':    {'B': 10000, 'A': 20000},
               'ALB':    {'A': 5000, 'B': 0, 'C': -35000, 'D': -100000, 'E': -150000, 'F': -300000} if size == 'medium' else {'B': -40000, 'A': 40000},
               'CTOdev': {'B': 0.5, 'A': 1.5, 'C': 2.5},
               'DID':    {'D': 50, 'C': 30, 'B': 10, 'A': 0},
               'CTO':    {'D': 3000, 'C': 10000, 'B': 40000, 'A': 80000}}

    # now make settings dictionary
    build_settings = {  'ALB': presets['ALB']['A'],         'NTO': presets['NTO']['A'],
                        'DID': presets['DID']['A'],         'CTO': presets['CTO']['A'],
                        'NDT': presets['NDT']['A'],         'CTOdev': presets['CTOdev']['A'],
                        'maxsize transaction': 1000,        'numtrans L3M': 1000,
                        'enddate': date.today(),            'ndays': 450}

    # set the file properties
    write_settings = {  'fmt': 'mt',
                        'path': './',
                        'filename': 'transaction_statement_'.format(presets['scorecard']),
                        'days per statement': build_settings['ndays'],
                        'IBAN': IBANlist[presets['bank']],
                        'print': True}

    # and start updating using the values from event
    apply_if_key_provided_and_valid('alb',      'albclass',    'ALB',    event, build_settings, presets)
    apply_if_key_provided_and_valid('nto',      'ntoclass',    'NTO',    event, build_settings, presets)
    apply_if_key_provided_and_valid('did',      'didclass',    'DID',    event, build_settings, presets)
    apply_if_key_provided_and_valid('cto',      'ctoclass',    'CTO',    event, build_settings, presets)
    apply_if_key_provided_and_valid('ndt',      'ndtclass',    'NDT',    event, build_settings, presets)
    apply_if_key_provided_and_valid('ctodev',   'ctodevclass', 'CTOdev', event, build_settings, presets,tpe='float')

    apply_if_key_provided_and_valid('transSize',    'noclass', 'maxsize transaction',   event, build_settings)
    apply_if_key_provided_and_valid('numtransL3M',  'noclass', 'numtrans L3M',          event, build_settings)
    apply_if_key_provided_and_valid('ndays',        'noclass', 'ndays',                 event, build_settings)

    if 'enddate' in event: build_settings['enddate'] = event['enddate']
    if 'format' in event and event['format'] in ['mt', 'camt']: write_settings['fmt'] = event['format']
    if 'iban' in event and event['iban'] in IBANlist: write_settings['IBAN'] = IBANlist[event['iban']]

    # build the file
    B = optimize_risk_drivers(build_settings, presets['scorecard'])
    result = build_statements(B, write_settings)
    # add the validation issues
    result.update({'values': validate(B)})

    # json and datetime dont play well, so convert to string before returning
    build_settings['enddate'] = str(build_settings['enddate'])
    # aaaaand return all stuff
    if 'download' in event and event['download']:
        mimetype = 'application/xml' if write_settings['fmt']=='camt' else 'text/plain'
        return return_file(200,result['file'],mimetype)
    return return_response(200,{"build settings": build_settings, "write settings": write_settings,   "body": result })

def return_response(code, val):
    return {
        'statusCode': code,
        'body': json.dumps(val),
        'headers': {'Content-Type': 'application/json'}}

def return_file(code, val,mimetype):
    return {
        'statusCode': code,
        'body': val,
        'headers': {'Content-Type': mimetype}}

class Statement:
    def __init__(self, IBAN='NL29INGB0642516537'):
        self.file = ''
        self.IBAN = IBAN
        self.transactionMT = ":61:{year}{month:02d}{day:02d}{month:02d}{day:02d}{crd_deb}{amount:.2f}N654NONREF\n"
        self.statementMT = '''{{1:F01ABNANL2AXXXX0000000000}}{{2:O9400000000000ABNANL2AXXXX00000000000000000000N}}{{3:}}{{4:
:20:{refnr:016d}
:25:{IBAN}
:28C:{refnr}/1
:60M:{stC/D}{year}{month:02d}{day:02d}EUR{startbal:.2f}
{transactions}:62M:{endC/D}{year}{month:02d}{day:02d}EUR{endbal:.2f}
-}}{{5:}}'''

    def _dateToStr(self, date):
        if type(date) == str:
            return date
        else:
            return date.strftime("%Y-%m-%d")

    def _print_MT_transaction(self, trdate, tramount):
        trans_settings = {
            'year': str(trdate.year)[-2:],
            'month': trdate.month,
            'day': trdate.day,
            'amount': abs(tramount)
        }
        trans_settings['crd_deb'] = 'D' if tramount < 0 else 'C'
        return self.transactionMT.format(**trans_settings).replace('.', ',')

    def _print_MT_statement(self, refnr, stdate, startbal, transactions, sumtrans):
        stmt_settings = {
            'refnr': refnr,
            'IBAN': self.IBAN,
            'year': str(stdate.year)[-2:],
            'month': stdate.month,
            'day': stdate.day,
            'startbal': abs(startbal),
            'endbal': abs(startbal + sumtrans),
            'transactions': transactions
        }
        stmt_settings['stC/D'] = 'D' if startbal < 0 else'C'
        stmt_settings['endC/D'] = 'D' if startbal + sumtrans < 0 else 'C'
        return self.statementMT.format(**stmt_settings).replace('.', ',')

    def _print_mt(self, timeline, transactions, startbal, endbal):
        refnr = 1  # 16digits
        intendbal = startbal
        for i in range(len(timeline)):
            intstartbal = intendbal
            tr, sumtr = '', 0
            for j in transactions:
                if j.date == timeline[i]:
                    tr += self._print_MT_transaction(j.date, j.amount)
                    sumtr += j.amount
            self.file += self._print_MT_statement(refnr, timeline[i], intstartbal, tr, sumtr)
            intendbal = intstartbal + sumtr
            refnr += 1

    def _print_camt(self, timeline, transactions, startbal, endbal):
        empty_camt = ET.Element('Document')
        # empty_camt.set("xmlns", "http://www.w3.org/2001/XMLSchema-instance")
        BkToCstmrStmt = ET.SubElement(empty_camt, 'BkToCstmrStmt')

        GrpHdr = ET.SubElement(BkToCstmrStmt, 'GrpHdr')
        ET.SubElement(GrpHdr, 'MsgId').text = '201703260000000_20170326170158591'
        ET.SubElement(GrpHdr, 'CreDtTm').text = '{0}'.format(datetime.datetime.utcnow())

        Stmt = ET.SubElement(BkToCstmrStmt, 'Stmt')

        ET.SubElement(Stmt, 'Id').text = '201703261701589650000001'
        ET.SubElement(Stmt, 'CreDtTm').text = '{0}'.format(datetime.datetime.utcnow())

        StmtFrToDt = ET.SubElement(Stmt, 'FrToDt')
        ET.SubElement(StmtFrToDt, 'FrDtTm').text = '{0}'.format(self._dateToStr(timeline[0]))
        ET.SubElement(StmtFrToDt, 'ToDtTm').text = '{0}'.format(self._dateToStr(timeline[-1]))

        Acct = ET.SubElement(Stmt, 'Acct')
        AcctId = ET.SubElement(Acct, 'Id')
        ET.SubElement(AcctId, 'IBAN').text = self.IBAN
        AcctTp = ET.SubElement(Acct, 'Tp')
        ET.SubElement(AcctTp, 'Cd').text = 'CACC'
        ET.SubElement(Acct, 'Ccy').text = 'EUR'
        AcctSvcr = ET.SubElement(Acct, 'Svcr')
        AcctFinInstnId = ET.SubElement(AcctSvcr, 'FinInstnId')
        ET.SubElement(AcctFinInstnId, 'BIC').text = 'INGBNL2A'

        ClBal = ET.SubElement(Stmt, 'Bal')
        ClBalTp = ET.SubElement(ClBal, 'Tp')
        ClBalCdOrPrtry = ET.SubElement(ClBalTp, 'CdOrPrtry')
        ET.SubElement(ClBalCdOrPrtry, 'Cd').text = 'OPBD'
        ET.SubElement(ClBal, 'Amt', Ccy="EUR").text = str(round(abs(startbal), 2))
        ET.SubElement(ClBal, 'CdtDbtInd').text = "{0}".format('CRDT' if startbal > 0 else 'DBIT')
        ClBalDt = ET.SubElement(ClBal, 'Dt')
        ET.SubElement(ClBalDt, 'Dt').text = '{0}'.format(self._dateToStr(timeline[0]))

        OpBal = ET.SubElement(Stmt, 'Bal')
        OpBalTp = ET.SubElement(OpBal, 'Tp')
        OpBalCdOrPrtry = ET.SubElement(OpBalTp, 'CdOrPrtry')
        ET.SubElement(OpBalCdOrPrtry, 'Cd').text = 'CLBD'
        ET.SubElement(OpBal, 'Amt', Ccy="EUR").text = str(round(abs(endbal), 2))
        ET.SubElement(OpBal, 'CdtDbtInd').text = "{0}".format('CRDT' if endbal > 0 else 'DBIT')
        OpBalDt = ET.SubElement(OpBal, 'Dt')
        ET.SubElement(OpBalDt, 'Dt').text = '{0}'.format(self._dateToStr(timeline[-1]))

        TxsSummry = ET.SubElement(Stmt, 'TxsSummry')

        s, n = 0, 0
        for i in transactions:
            s += abs(i.amount)
            n += 1
        TtlNtries = ET.SubElement(TxsSummry, 'TtlNtries')
        ET.SubElement(TtlNtries, 'NbOfNtries').text = '{0}'.format(n)
        ET.SubElement(TtlNtries, 'Sum').text = '{0:.2f}'.format(s)

        s, n = 0, 0
        for i in transactions:
            if i.amount >= 0:
                s += abs(i.amount)
                n += 1
        TtlCdtNtries = ET.SubElement(TxsSummry, 'TtlCdtNtries')
        ET.SubElement(TtlCdtNtries, 'NbOfNtries').text = '{0}'.format(n)
        ET.SubElement(TtlCdtNtries, 'Sum').text = '{0:.2f}'.format(s)

        s, n = 0, 0
        for i in transactions:
            if i.amount < 0:
                s += abs(i.amount)
                n += 1
        TtlDbtNtries = ET.SubElement(TxsSummry, 'TtlDbtNtries')
        ET.SubElement(TtlDbtNtries, 'NbOfNtries').text = '{0}'.format(n)
        ET.SubElement(TtlDbtNtries, 'Sum').text = '{0:.2f}'.format(s)

        for i in transactions:
            Ntry = ET.SubElement(Stmt, 'Ntry')
            ET.SubElement(Ntry, 'NtryRef').text = '1234567890'
            ET.SubElement(Ntry, 'Amt', Ccy="EUR").text = '{0:.2f}'.format(abs(i.amount))
            ET.SubElement(Ntry, 'CdtDbtInd').text = "{0}".format('CRDT' if i.amount > 0 else 'DBIT')
            ET.SubElement(Ntry, 'Sts').text = 'BOOK'
            BookgDt = ET.SubElement(Ntry, 'BookgDt')
            ET.SubElement(BookgDt, 'Dt').text = '{0}'.format(self._dateToStr(i.date))
            ValDt = ET.SubElement(Ntry, 'ValDt')
            ET.SubElement(ValDt, 'Dt').text = '{0}'.format(self._dateToStr(i.date))
            ET.SubElement(Ntry, 'AcctSvcrRef').text = '1234567890'

        b = """<Document xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"
              xmlns="urn:iso:std:iso:20022:tech:xsd:camt.053.001.02"
              xsi:schemaLocation="urn:iso:std:iso:20022:tech:xsd:camt.053.001.02 camt.053.001.02.xsd">"""

        self.file += MD.parseString(ET.tostring(empty_camt, encoding='utf8', method='xml')).toprettyxml(
            indent="\t").replace('<Document>', b)

    def print_file(self, timeline, transactions, startbal, endbal, fmt):
        if fmt == 'mt':
            self._print_mt(timeline, transactions, startbal, endbal)
        else:
            self._print_camt(timeline, transactions, startbal, endbal)

class Timeline:
    def __init__(self, enddate=date.today(), ndays=450):
        self.list, day = [], self._strToDate(enddate)
        for i in range(ndays):
            day -= timedelta(1)
            self.list.append(day)
        self.list.reverse()

    def _strToDate(self, date):
        if type(date) == str:
            return datetime.datetime.strptime(date, "%Y-%m-%d")
        else:
            return date

    def _last_day_of_month(self, any_day):
        next_month = any_day.replace(day=28) + datetime.timedelta(days=4)
        return next_month - datetime.timedelta(days=next_month.day)

    def get_full_month(self, nr=None):
        sliced, months = {}, []
        for i in self.list:
            if (i.year, i.month) not in months:
                sliced[(i.year, i.month)] = [i]
                months.append((i.year, i.month))
            else:
                sliced[(i.year, i.month)].append(i)
        full_months = [sliced[i] for i in months if
                       sliced[i][-1] == self._last_day_of_month(sliced[i][-1]) and sliced[i][0].day == 1]
        if not nr: return full_months
        return full_months[nr]

    def get_st_and_end(self, period, index=True):
        period_map = {'L1M': (-1, -1), 'L2M': (-2, -1), 'L3M': (-3, -1), 'L6M': (-6, -1),
                      'F1M': (-13, -13), 'F2M': (-14, -13), 'F3M': (-15, -13)}
        if index:
            return (self.list.index(self.get_full_month(period_map[period][0])[0]),
                    self.list.index(self.get_full_month(period_map[period][1])[-1]))
        return (self.get_full_month(period_map[period][0])[0],
                self.get_full_month(period_map[period][1])[-1])

class datedValue:
    def __init__(self, date, amount):
        self.date = date
        self.amount = amount
        self.size = abs(amount)
        self.sign = -1 if amount < 0 else 1
        self.CTDT = 'CRDT' if amount > 0 else 'DBIT'

class BankTransactions:
    def __init__(self, ok_bk, enddate=date.today(), ndays=450):
        self.timeline = Timeline(enddate, ndays)
        self.transactions = []
        self.eodBalances = []
        self.startBal = None
        self.type = ok_bk

    def getEODbalances(self, period, startbal=0, Sorted=False):
        self._updateEODbal(startbal)

        if period == 'all':
            if Sorted: return sorted(self.eodBalances, key=lambda val: val.amount)
            return self.eodBalances
        else:
            (startindex, endindex) = self.timeline.get_st_and_end(period)
            if Sorted: return sorted(self.eodBalances[startindex:endindex + 1], key=lambda val: val.amount)
            return self.eodBalances[startindex:endindex + 1]

    def _updateEODbal(self, startbal=0):
        self.startBal = startbal
        balance = startbal
        self.eodBalances = []
        for i in self.timeline.list:
            for j in self.transactions:
                if i == j.date:
                    balance += j.amount
            self.eodBalances.append(datedValue(i, balance))

    def get_days_in_debt(self, period, startbal=0):
        (startindex, endindex) = self.timeline.get_st_and_end(period)
        self._updateEODbal(startbal)
        return len([i.amount for i in self.eodBalances[startindex:endindex] if i.amount < 0])

    def add_transaction(self, amount, date):
        self.transactions.append(datedValue(date, amount))

    def add_transactions(self, amounts, period=None, day=None):
        if period:
            (startindex, endindex) = self.timeline.get_st_and_end(period)
            subTimeline = self.timeline.list[startindex:endindex + 1]
        elif day:
            subTimeline = [self.timeline.list[day]]
        for i in amounts:
            self.transactions.append(datedValue(subTimeline[randint(0, len(subTimeline) - 1)], i))

def apply_if_key_provided_and_valid(key,class_key,target_key,event,settings,presets=None,tpe='int'):
    if class_key in event:
        if presets:
            if event[class_key] in presets[target_key]:
                settings[target_key]=presets[target_key][event[class_key]]
    elif key in event:
        if tpe=='int':
            settings[target_key] = int(event[key])
        elif tpe=='float':
            settings[target_key] = float(event[key])

def transaction_random_credit_fixed_sum(cto, maxsize=1000):
    transactions = []
    while sum(transactions) < cto - maxsize:
        transactions.append(randint(0, maxsize))
    transactions.append(cto - sum(transactions))
    return transactions

def transactions_random_neutral_balance(num, maxsize=1000):
    transactions = []
    for i in range(int(num / 2)):
        val = randint(0, maxsize)
        transactions.append(val)
        transactions.append(-val)
    return transactions

def transactions_random_debit_fixed_number(ndt, minsize=-1000):
    return [randint(minsize, -1) for i in range(ndt)]

def transaction_optimize_albL6M(B, alb, startbalL3M, maxsize=1000):
    numdaysL6M = len(B.getEODbalances('L6M'))
    EODbalL3M = [i.amount for i in B.getEODbalances('L3M', startbalL3M)]
    sumEODbalM4to6 = alb * numdaysL6M - sum(EODbalL3M)
    EODbalM4to6 = (sumEODbalM4to6 - startbalL3M) / (numdaysL6M - len(EODbalL3M) - 1)
    transaction = int(startbalL3M - EODbalM4to6)
    transactions = []
    while abs(sum(transactions)) < abs(transaction) - maxsize:
        transactions.append(randint(0, maxsize)) if transaction > 0 else transactions.append(randint(-maxsize, 0))
    transactions.append(transaction - sum(transactions))
    startbalL6M = startbalL3M - transaction
    return transactions, startbalL6M

def transactions_optimize_cto_dev(B, cto_dev, maxsize=1000):
    nmonths = len(B.timeline.get_full_month())
    if nmonths >= 15:
        period = ('L3M', 'F3M')
    elif nmonths == 14:
        period = ('L2M', 'F2M')
    elif nmonths == 13:
        period = ('L1M', 'F1M')
    cto = sum([i.amount for i in B.transactions if i.amount > 0
               and i.date >= B.timeline.get_st_and_end(period[0], index=False)[0]
               and i.date <= B.timeline.get_st_and_end(period[0], index=False)[1]])
    return transaction_random_credit_fixed_sum(int(cto / (cto_dev)), maxsize=maxsize), period[1]

def optimize_risk_drivers(s, size):
    if size == 'small':
        B = BankTransactions('small', s['enddate'], s['ndays'])
        B.add_transactions(transaction_random_credit_fixed_sum(s['CTO'] * 3, maxsize=s['maxsize transaction']),
                           period='L3M')
        B.add_transactions(transactions_random_debit_fixed_number(s['NDT'], minsize=-s['maxsize transaction']),
                           period='L3M')

        startbal = -[i.amount for i in B.getEODbalances('L3M', Sorted=True)][s['DID']]
        transactionsM4lD, startbal = transaction_optimize_albL6M(B, s['ALB'], startbal,
                                                                 maxsize=s['maxsize transaction'])
        B.add_transactions(transactionsM4lD, day=B.timeline.get_st_and_end('L3M')[0] - 1)

        cto_dev_transactions, period_Y_1 = transactions_optimize_cto_dev(B, s['CTOdev'],
                                                                         maxsize=s['maxsize transaction'])
        startbal = startbal - sum(cto_dev_transactions)
        B.add_transactions(cto_dev_transactions, period=period_Y_1)
        B._updateEODbal(startbal)
    elif size == 'medium':
        B = BankTransactions('medium', s['enddate'], s['ndays'])
        tr = transaction_random_credit_fixed_sum(s['NTO'] * 3, maxsize=s['maxsize transaction'])
        if len(tr) < s['numtrans L3M']:
            tr += transactions_random_neutral_balance(s['numtrans L3M'] - len(tr), maxsize=s['maxsize transaction'])
        B.add_transactions(tr, period='L3M')
        eodBals = [i.amount for i in B.getEODbalances('L3M')]
        initial_ALB = sum(eodBals) / len(eodBals)
        B._updateEODbal(s['ALB'] - initial_ALB)
    return B

def validate(B):
    results = {}
    nmonths = len(B.timeline.get_full_month())

    if B.type == 'small':
        if nmonths >= 15:
            period = ('L3M', 'F3M')
        elif nmonths == 14:
            period = ('L2M', 'F2M')
        elif nmonths == 13:
            period = ('L1M', 'F1M')

        CTOLM = sum([i.amount for i in B.transactions if i.amount > 0
                     and i.date >= B.timeline.get_st_and_end(period[0], index=False)[0]
                     and i.date <= B.timeline.get_st_and_end(period[0], index=False)[1]])
        CTOFM = sum([i.amount for i in B.transactions if i.amount > 0
                     and i.date >= B.timeline.get_st_and_end(period[1], index=False)[0]
                     and i.date <= B.timeline.get_st_and_end(period[1], index=False)[1]])
        if CTOLM == 0:
            results['cto_Development_XM'] = 0
        else:
            results['cto_Development_XM'] = round(CTOLM / CTOFM, 2)
        results['Average_Monthly_Credit_Turnover_3M'] = int(sum([i.amount for i in B.transactions if i.amount > 0
                                                                 and i.date >=
                                                                 B.timeline.get_st_and_end('L3M', index=False)[0]
                                                                 and i.date <=
                                                                 B.timeline.get_st_and_end('L3M', index=False)[1]]) / 3)
        results['Number_of_Debit_Transactions_3M'] = len([i.amount for i in B.transactions if i.amount < 0
                                                          and i.date >= B.timeline.get_st_and_end('L3M', index=False)[0]
                                                          and i.date <= B.timeline.get_st_and_end('L3M', index=False)[
                                                              1]])
        results['Number_of_days_in_debit_3M'] = len([i.amount for i in B.eodBalances if i.amount < 0
                                                     and i.date >= B.timeline.get_st_and_end('L3M', index=False)[0]
                                                     and i.date <= B.timeline.get_st_and_end('L3M', index=False)[1]])
        EODbalL6M = [i.amount for i in B.eodBalances
                     if i.date >= B.timeline.get_st_and_end('L6M', index=False)[0]
                     and i.date <= B.timeline.get_st_and_end('L6M', index=False)[1]]
        results['Average_Ledger_Balance_6M'] = int(sum(EODbalL6M) / len(EODbalL6M))
    elif B.type == 'medium':
        nmonths = len(B.timeline.get_full_month())
        results['AVG_Net_TO'] = int(sum([i.amount for i in B.transactions
                                         if i.date >= B.timeline.get_st_and_end('L3M', index=False)[0]
                                         and i.date <= B.timeline.get_st_and_end('L3M', index=False)[1]]) / 3)
        EODbalL3M = [i.amount for i in B.eodBalances
                     if i.date >= B.timeline.get_st_and_end('L3M', index=False)[0]
                     and i.date <= B.timeline.get_st_and_end('L3M', index=False)[1]]
        results['AVG_LEDG_BAL'] = int(sum(EODbalL3M) / len(EODbalL3M))

    return results

def write_file_content(timeline, transactions, startbal, endbal, IBAN='NL29INGB0642516537', fmt='camt'):
    if len(transactions) > 0:
        ST = Statement(IBAN)
        ST.print_file(timeline, transactions, startbal, endbal, fmt)
        return ST.file
    return None

def build_statements(B, settings):
    '''
    Build the full file. Writes either a zip with multiple files, of returns just one file in the desire format
    :param settings:
    {
        'fmt':'mt',
        'path':'/Users/ruben/Desktop/DEL2/',
        'filename':'transaction_statement',
        'days per statement':1,
        'IBAN':'NL29INGB0642516537',
        'print':False
    }
    '''

    # set some values to start with:
    # we'll be collecting data to write in them, and reset at every new file
    day_in_statement, entries_to_include, current_timeline = 0, [], []
    # keeping track of the file number
    files = []
    # also keeping track of the start and end balances (end balance is for now set to start balance)
    current_startbal, current_endbal = B.startBal, B.startBal
    # and setting the extension, xml for camt (default) txt for MT940 files
    extension = 'txt' if settings['fmt'] == 'mt' else 'xml'
    timeline = copy.deepcopy(B.timeline.list)

    # now iterate over days, and keep going as long as we haven't maxed out the number of days in a file
    while day_in_statement < settings['days per statement'] and len(timeline) != 0:
        # as long as there is a day to be written out:
        if len(timeline) != 0:
            # read and remove the day we will deal with now
            day = timeline.pop(0)
            # add the day to the current file
            current_timeline.append(day)
            # and add all transactions corresponding to that day
            day_statements = [i for i in B.transactions if i.date == day]
            entries_to_include += day_statements
            # and increment the number of days included in the file
            day_in_statement += 1

        # if the end of the timeline is reached, or the max number of days per file is reached:
        if day_in_statement == settings['days per statement'] or len(timeline) == 0:
            # calculate the end balance from the data
            current_endbal += sum([i.amount for i in entries_to_include])
            # and generate a formatted file
            files.append(write_file_content(current_timeline, entries_to_include, current_startbal, current_endbal,
                                            fmt=settings['fmt'], IBAN=settings['IBAN']))
            # update the start balance for the next file (equals the current end balance)
            current_startbal = current_endbal
            # and reset all the local arrays and counters
            day_in_statement, entries_to_include, current_timeline = 0, [], []

    # if more than file is made, a zip will be generated
    if len(files) > 1:
        # get the zip initialized
        mf = io.BytesIO()
        zf = zipfile.ZipFile(mf, mode='w', compression=zipfile.ZIP_DEFLATED)
        for f in files:
            if f:
                zf.writestr('{0}_{1}.{2}'.format(settings['filename'], files.index(f) + 1, extension), f)
        zf.close()
        outfile = mf.getvalue()
        extension = 'zip'
        writestyle = "wb"
    elif len(files) == 1:
        outfile = files[0]
        writestyle = "w"

    if not settings['print']:
        with open('{0}{1}.{2}'.format(settings['path'], settings['filename'], extension), writestyle) as g:
            g.write(outfile)
    else:
        return {'file': outfile, 'extension': extension}